import _ from "lodash"

export default function ({ $axios, $cookies, store }, inject) {
  const appApi = $axios.create({
    baseURL: process.env.api_url,
    withCredentials: true,
  })

  const validateRequest = (error) => {
    // const messages = _.get(error, "message", "").split("code ")
    // if (messages[1] === "401" && process.client) {
    //   console.log(`${oho_key} error 401`, error)
    //   return (window.location = "/login?action=token_expired")
    // }
  }

  const setToken = () => {
    //  config.headers.common["Authorization"]
  }

  appApi.onResponseError((error) => validateRequest(error))
  appApi.onRequest((config) => setToken())

  inject("appApi", appApi)
}
